package unit_tests

import (
	"gitlab.com/atrico/syncEx"
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
	"testing"
)

func Test_ContextValues(t *testing.T) {
	// Arrange
	ctx := syncEx.NewBackgroundContextWithValues()
	vals1 := map[any]any{"key1": "value1", "key2": "value2"}
	vals2 := map[any]any{"key3": "value3", "key4": "value4"}

	// Act
	for k, v := range vals1 {
		ctx.AddValue(k, v)
	}
	ctx.AddValues(vals2)

	// Assert
	for k, v := range vals1 {
		assert.That(t, ctx.Value(k), is.EqualTo(v), "%v", k)
	}
	for k, v := range vals2 {
		assert.That(t, ctx.Value(k), is.EqualTo(v), "%v", k)
	}
}
