package unit_tests

import (
	"context"
	"fmt"
	"reflect"
	"testing"
	"time"

	"gitlab.com/atrico/syncEx"
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
)

var testMergeWaitableTimeout = 250 * time.Millisecond

func Test_MergeWaitable_Single(t *testing.T) {
	// Arrange
	event0 := syncEx.NewEvent()
	merged := syncEx.MergeWaitables(namedWaits("event", event0))

	// Act
	go func() { event0.Set() }()
	ctx, cancelFunc := context.WithTimeout(context.Background(), testMergeWaitableTimeout)
	defer cancelFunc()
	id, err := merged.WaitForValue(ctx)

	// Assert
	assert.That[any](t, err, is.Nil, "No error")
	assert.That(t, id.(string), is.EqualTo("event0"), "Correct Id")
}

func Test_MergeWaitable_Multiple(t *testing.T) {
	// Arrange
	event0 := syncEx.NewEvent()
	event1 := syncEx.NewEvent()
	event2 := syncEx.NewEvent()
	merged := syncEx.MergeWaitables(namedWaits("event", event0, event1, event2))

	// Act
	go func() { event1.Set() }()
	ctx, cancelFunc := context.WithTimeout(context.Background(), testMergeWaitableTimeout)
	defer cancelFunc()
	id, err := merged.WaitForValue(ctx)

	// Assert
	assert.That[any](t, err, is.Nil, "No error")
	assert.That(t, id.(string), is.EqualTo("event1"), "Correct Id")
}

func Test_MergeWaitable_Many(t *testing.T) {
	// Arrange
	const count = 128
	events := make([]syncEx.Waitable, 0, count)
	for i := 0; i < count; i++ {
		events = append(events, syncEx.NewEvent())
	}
	merged := syncEx.MergeWaitables(namedWaits("event", events...))

	// Act
	triggered := count / 2
	go func() { events[triggered].(syncEx.Event).Set() }()
	ctx, cancelFunc := context.WithTimeout(context.Background(), testMergeWaitableTimeout)
	defer cancelFunc()
	id, err := merged.WaitForValue(ctx)

	// Assert
	assert.That[any](t, err, is.Nil, "No error")
	assert.That(t, id.(string), is.EqualTo(fmt.Sprintf("event%d", triggered)), "Correct Id")
}

func Test_MergeWaitable_Empty(t *testing.T) {
	// Arrange
	merged := syncEx.MergeWaitables(namedWaits("event"))

	// Act
	ctx, cancelFunc := context.WithTimeout(context.Background(), testMergeWaitableTimeout)
	defer cancelFunc()
	_, err := merged.WaitForValue(ctx)

	// Assert
	assert.That[any](t, err, is.Type(reflect.TypeOf(context.DeadlineExceeded)), "Error")
}

func Test_MergeWaitableWithValue_Single(t *testing.T) {
	// Arrange
	promise0 := syncEx.NewPromise()
	merged := syncEx.MergeWaitablesWithValue(namedWaitWithValues("promise", promise0))
	expValue := rg.String()

	// Act
	go func() { promise0.Fulfil(expValue) }()
	ctx, cancelFunc := context.WithTimeout(context.Background(), testMergeWaitableTimeout)
	defer cancelFunc()
	val, err := merged.WaitForValue(ctx)
	idAndValue := val.(syncEx.IdAndValue)

	// Assert
	assert.That[any](t, err, is.Nil, "No error")
	assert.That(t, idAndValue.Id, is.EqualTo("promise0"), "Correct Id")
	assert.That(t, idAndValue.Value.(string), is.EqualTo(expValue), "Correct Value")
}

func Test_MergeWaitableWithValue_Multiple(t *testing.T) {
	// Arrange
	promise0 := syncEx.NewPromise()
	promise1 := syncEx.NewPromise()
	promise2 := syncEx.NewPromise()
	merged := syncEx.MergeWaitablesWithValue(namedWaitWithValues("promise", promise0, promise1, promise2))
	expValue := rg.String()

	// Act
	go func() { promise1.Fulfil(expValue) }()
	ctx, cancelFunc := context.WithTimeout(context.Background(), testMergeWaitableTimeout)
	defer cancelFunc()
	val, err := merged.WaitForValue(ctx)
	idAndValue := val.(syncEx.IdAndValue)

	// Assert
	assert.That[any](t, err, is.Nil, "No error")
	assert.That(t, idAndValue.Id, is.EqualTo("promise1"), "Correct Id")
	assert.That(t, idAndValue.Value.(string), is.EqualTo(expValue), "Correct Value")
}

func Test_MergeWaitableWithValue_Many(t *testing.T) {
	// Arrange
	const count = 128
	promises := make([]syncEx.WaitableWithValue, 0, count)
	for i := 0; i < count; i++ {
		promises = append(promises, syncEx.NewPromise())
	}
	merged := syncEx.MergeWaitablesWithValue(namedWaitWithValues("promise", promises...))
	expValue := rg.String()

	// Act
	triggered := count / 2
	go func() { promises[triggered].(syncEx.Promise).Fulfil(expValue) }()
	ctx, cancelFunc := context.WithTimeout(context.Background(), testMergeWaitableTimeout)
	defer cancelFunc()
	val, err := merged.WaitForValue(ctx)
	idAndValue := val.(syncEx.IdAndValue)

	// Assert
	assert.That[any](t, err, is.Nil, "No error")
	assert.That(t, idAndValue.Id, is.EqualTo(fmt.Sprintf("promise%d", triggered)), "Correct Id")
	assert.That(t, idAndValue.Value.(string), is.EqualTo(expValue), "Correct Value")
}

func Test_MergeWaitableWithValue_Empty(t *testing.T) {
	// Arrange
	merged := syncEx.MergeWaitablesWithValue(namedWaitWithValues("promise"))

	// Act
	ctx, cancelFunc := context.WithTimeout(context.Background(), testMergeWaitableTimeout)
	defer cancelFunc()
	_, err := merged.WaitForValue(ctx)

	// Assert
	assert.That[any](t, err, is.Type(reflect.TypeOf(context.DeadlineExceeded)), "Error")
}

// ----------------------------------------------------------------------------------------------------------------------------
// Benchmarks
// ----------------------------------------------------------------------------------------------------------------------------

func Benchmark_MergeWaitable_Single(b *testing.B) {
	benchmarkMergeWaitable(b, syncEx.ImmediateWaitable)
}

func Benchmark_MergeWaitable_Multiple(b *testing.B) {
	benchmarkMergeWaitable(b, syncEx.InfiniteWaitable, syncEx.ImmediateWaitable, syncEx.InfiniteWaitable)
}

func Benchmark_MergeWaitable_Many(b *testing.B) {
	const count = 128
	const triggered = count / 2
	w := make([]syncEx.Waitable, 0, count)
	for i := 0; i < count; i++ {
		if count == triggered {
			w = append(w, syncEx.ImmediateWaitable)
		} else {
			w = append(w, syncEx.InfiniteWaitable)
		}
	}
	benchmarkMergeWaitable(b, w...)
}

func Benchmark_MergeWaitableWithValue_Single(b *testing.B) {
	benchmarkMergeWaitableWithValue(b, syncEx.ImmediateWaitableWithValue)
}

func Benchmark_MergeWaitableWithValue_Multiple(b *testing.B) {
	benchmarkMergeWaitableWithValue(b, syncEx.InfiniteWaitableWithValue, syncEx.ImmediateWaitableWithValue, syncEx.InfiniteWaitableWithValue)
}

func Benchmark_MergeWaitableWithValue_Many(b *testing.B) {
	const count = 128
	const triggered = count / 2
	w := make([]syncEx.WaitableWithValue, 0, count)
	for i := 0; i < count; i++ {
		if count == triggered {
			w = append(w, syncEx.ImmediateWaitableWithValue)
		} else {
			w = append(w, syncEx.InfiniteWaitableWithValue)
		}
	}
	benchmarkMergeWaitableWithValue(b, w...)
}

var val interface{}
var err error

func benchmarkMergeWaitable(b *testing.B, w ...syncEx.Waitable) {
	waits := namedWaits("waitable", w...)
	for n := 0; n < b.N; n++ {
		merged := syncEx.MergeWaitables(waits)
		ctx, cancelFunc := context.WithTimeout(context.Background(), testMergeWaitableTimeout)
		val, err = merged.WaitForValue(ctx)
		cancelFunc()
	}
}

func benchmarkMergeWaitableWithValue(b *testing.B, w ...syncEx.WaitableWithValue) {
	waits := namedWaitWithValues("waitable", w...)
	for n := 0; n < b.N; n++ {
		merged := syncEx.MergeWaitablesWithValue(waits)
		ctx, cancelFunc := context.WithTimeout(context.Background(), testMergeWaitableTimeout)
		val, err = merged.WaitForValue(ctx)
		cancelFunc()
	}
}

func namedWaits(prefix string, waits ...syncEx.Waitable) (named map[string]syncEx.Waitable) {
	named = make(map[string]syncEx.Waitable, len(waits))
	for i, wait := range waits {
		named[fmt.Sprintf("%s%d", prefix, i)] = wait
	}
	return named
}

func namedWaitWithValues(prefix string, waits ...syncEx.WaitableWithValue) (named map[string]syncEx.WaitableWithValue) {
	named = make(map[string]syncEx.WaitableWithValue, len(waits))
	for i, wait := range waits {
		named[fmt.Sprintf("%s%d", prefix, i)] = wait
	}
	return named
}
