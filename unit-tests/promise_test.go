package unit_tests

import (
	"context"
	"fmt"
	"reflect"
	"testing"

	"gitlab.com/atrico/syncEx"
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
)

func Test_Promise_NoValue(t *testing.T) {
	// Arrange
	promise := syncEx.NewPromise()

	// Act
	_, err := promise.WaitForValue(createContext())
	fmt.Println(err)

	// Assert
	assert.That[any](t, err, is.Type(reflect.TypeOf(context.DeadlineExceeded)), "Error")
}

func Test_Promise_HasValue(t *testing.T) {
	// Arrange
	promise := syncEx.NewPromise()
	expected := rg.String()

	// Act
	promise.Fulfil(expected)
	value, err := promise.WaitForValue(createContext())
	fmt.Println(value)

	// Assert
	assert.That[any](t, err, is.Nil, "No Error")
	assert.That(t, value.(string), is.EqualTo(expected), "Correct value")
}
