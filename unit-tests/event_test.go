package unit_tests

import (
	"context"
	"testing"

	"gitlab.com/atrico/syncEx"
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
)

func Test_Event_ManualInitiallyReset(t *testing.T) {
	// Arrange
	event := syncEx.NewEvent()

	// Act
	status1 := event.IsSet()
	result1 := event.Wait(createContext())

	// Assert
	assert.That(t, status1, is.False, "1st IsSet not set")
	assert.That(t, result1, is.EqualTo(context.DeadlineExceeded), "1st Event reset")
}

func Test_Event_ManualInitiallyResetThenSet(t *testing.T) {
	// Arrange
	event := syncEx.NewEvent()

	// Act
	set := event.Set()
	status1 := event.IsSet()
	result1 := event.Wait(createContext())
	status2 := event.IsSet()
	result2 := event.Wait(createContext())

	// Assert
	assert.That(t, set, is.True, "Set change")
	assert.That(t, status1, is.True, "1st IsSet set")
	assert.That[any](t, result1, is.Nil, "1st Event set")
	assert.That(t, status2, is.True, "2nd IsSet set")
	assert.That[any](t, result2, is.Nil, "2nd Event set")
}

func Test_Event_ManualInitiallyResetThenReset(t *testing.T) {
	// Arrange
	event := syncEx.NewEvent()

	// Act
	status1 := event.IsSet()
	reset := event.Reset()
	status2 := event.IsSet()
	result1 := event.Wait(createContext())

	// Assert
	assert.That(t, status1, is.False, "1st IsSet not set")
	assert.That(t, reset, is.False, "Reset no change")
	assert.That(t, status2, is.False, "2nd IsSet not set")
	assert.That(t, result1, is.EqualTo(context.DeadlineExceeded), "1st Event reset")
}

func Test_Event_ManualInitiallySet(t *testing.T) {
	// Arrange
	event := syncEx.NewEvent()
	event.Set()

	// Act
	status1 := event.IsSet()
	result1 := event.Wait(createContext())
	status2 := event.IsSet()
	result2 := event.Wait(createContext())

	// Assert
	assert.That(t, status1, is.True, "1st IsSet set")
	assert.That[any](t, result1, is.Nil, "1st Event set")
	assert.That(t, status2, is.True, "2nd IsSet set")
	assert.That[any](t, result2, is.Nil, "2nd Event set")
}

func Test_Event_ManualInitiallySetThenSet(t *testing.T) {
	// Arrange
	event := syncEx.NewEvent()
	event.Set()

	// Act
	set := event.Set()
	result1 := event.Wait(createContext())
	result2 := event.Wait(createContext())

	// Assert
	assert.That(t, set, is.False, "Set no change")
	assert.That[any](t, result1, is.Nil, "1st Event set")
	assert.That[any](t, result2, is.Nil, "2nd Event set")
}

func Test_Event_ManualInitiallySetThenReset(t *testing.T) {
	// Arrange
	event := syncEx.NewEvent()
	event.Set()

	// Act
	reset := event.Reset()
	result1 := event.Wait(createContext())

	// Assert
	assert.That(t, reset, is.True, "Reset change")
	assert.That(t, result1, is.EqualTo(context.DeadlineExceeded), "1st Event reset")
}
func Test_Event_AutoInitiallyReset(t *testing.T) {
	// Arrange
	event := syncEx.NewAutoResetEvent()

	// Act
	result1 := event.Wait(createContext())

	// Assert
	assert.That(t, result1, is.EqualTo(context.DeadlineExceeded), "1st Event reset")
}

func Test_Event_AutoInitiallyResetThenSet(t *testing.T) {
	// Arrange
	event := syncEx.NewAutoResetEvent()

	// Act
	set := event.Set()
	result1 := event.Wait(createContext())
	result2 := event.Wait(createContext())

	// Assert
	assert.That(t, set, is.True, "Set change")
	assert.That[any](t, result1, is.Nil, "1st Event set")
	assert.That(t, result2, is.EqualTo(context.DeadlineExceeded), "2nd Event reset")
}

func Test_Event_AutoInitiallyResetThenReset(t *testing.T) {
	// Arrange
	event := syncEx.NewAutoResetEvent()

	// Act
	reset := event.Reset()
	result1 := event.Wait(createContext())

	// Assert
	assert.That(t, reset, is.False, "Reset no change")
	assert.That(t, result1, is.EqualTo(context.DeadlineExceeded), "1st Event reset")
}

func Test_Event_AutoInitiallySet(t *testing.T) {
	// Arrange
	event := syncEx.NewAutoResetEvent()
	event.Set()

	// Act
	result1 := event.Wait(createContext())
	result2 := event.Wait(createContext())

	// Assert
	assert.That[any](t, result1, is.Nil, "1st Event set")
	assert.That(t, result2, is.EqualTo(context.DeadlineExceeded), "2nd Event reset")
}

func Test_Event_AutoInitiallySetThenSet(t *testing.T) {
	// Arrange
	event := syncEx.NewAutoResetEvent()
	event.Set()

	// Act
	set := event.Set()
	result1 := event.Wait(createContext())
	result2 := event.Wait(createContext())

	// Assert
	assert.That(t, set, is.False, "Set no change")
	assert.That[any](t, result1, is.Nil, "1st Event set")
	assert.That(t, result2, is.EqualTo(context.DeadlineExceeded), "2nd Event reset")
}

func Test_Event_AutoInitiallySetThenReset(t *testing.T) {
	// Arrange
	event := syncEx.NewAutoResetEvent()
	event.Set()

	// Act
	reset := event.Reset()
	result1 := event.Wait(createContext())

	// Assert
	assert.That(t, reset, is.True, "Reset change")
	assert.That(t, result1, is.EqualTo(context.DeadlineExceeded), "1st Event reset")
}
