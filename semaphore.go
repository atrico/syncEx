package syncEx

import (
	"context"
)

// Semaphore synchronisation object
// Limit on number of "active" holders
// Wait will block when Limit is reached
type Semaphore interface {
	Waitable
	// Release one count
	Release()
	// Current status of semaphore
	Status() (current, limit int)
}

func NewSemaphore(limit int) Semaphore {
	return &semaphore{make(chan struct{}, limit)}
}

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------------------------------------------------------

type semaphore struct {
	ch chan struct{}
}

func (s *semaphore) Wait(ctx context.Context) error {
	select {
	case s.ch <- struct{}{}:
	case <-ctx.Done():
	}
	return ctx.Err()
}

func (s *semaphore) Release() {
	select {
	case <-s.ch:
	default:
		panic("too many releases")
	}
}

func (s *semaphore) Status() (current, limit int) {
	return len(s.ch), cap(s.ch)
}
